/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shape;

/**
 *
 * @author Natthakritta
 */
public class Circle extends Shape {

    private double r;
    private double PI = 22.0 / 7;

    public Circle(double r) {
        super("Circle");
        this.r = r;
    }

    @Override
    public double calArea() {
        return PI * r * r;
    }

    public String toString() {
        return "calArea Circle " + calArea();
    }
}
