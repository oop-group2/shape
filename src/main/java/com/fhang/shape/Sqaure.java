/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shape;

/**
 *
 * @author Natthakritta
 */
public class Sqaure extends Shape{
    private double s;

    public Sqaure(double s) {
        super("Sqaure");
        this.s = s;
    }
    @Override
    public double calArea() {
        return s*s;
    }
    
    public String toString() {
        return "calArea Sqaure "+calArea();
    }
    
}
