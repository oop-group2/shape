/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.shape;

/**
 *
 * @author Natthakritta
 */
public class Rectangle extends Shape {

    private double w;
    private double h;

    public Rectangle(double w, double h) {
        super("Rectangle");
        this.w = w;
        this.h = h;
    }

    @Override
    public double calArea() {
        return w * h;
    }

  
    public String toString() {
        return "calArea Rectangle "+calArea();
    }

    

    

}
